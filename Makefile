ADB=~/Android/Sdk/platform-tools/adb
APK=./app/build/outputs/apk/debug/app-debug.apk

.PHONY: build doc

build:
	./gradlew build

deploy:
	$(ADB) install -r $(APK)

install: build deploy
	@echo "Done."

check:
	./gradlew test --warning-mode all

clean:
	./gradlew clean

log:
	$(ADB) logcat |grep TCC

log-full:
	$(ADB) logcat

doc:
	doxygen

