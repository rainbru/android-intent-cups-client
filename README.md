# android-intent-cups-client

Print from android jpeg intent filter to network cups printers

This is **not** an android print service. Instead, this application
answers to the *jpeg* intent filter and send file to a preselected network 
**CUPS** printer.

# Screenshots

[![Settings screenShot](doc/screenshots/TCC-settings-tn.png)](doc/screenshots/TCC-settings.png?raw=true)
[![Main activity screenShot](doc/screenshots/TCC-main-tn.png)](doc/screenshots/TCC-main.png?raw=true)

# Build

You must have the android sdk installed in `~/Android/Sdk/`. The simplest is
maybe to download *android studio* from https://developer.android.com/studio 
It will download the newest SDK at first launch.

On installed, build and compile the java code :

	make

This will download needed gradlew version and start build.

# Doxygen Documentation

	make doc
	chromium html/index.html
